* Ehsan Jahangirzadeh 810194554
* CA4 clocked SR latch 

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=45n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1.1V
+beta=2
+Wp='8*Lmin*beta'
+Wn='2*Lmin'
.temp   25

**** Source Voltage ****

VSupply		Vs		GND		DC		Vdd
** signal C  with 4ns period
Vc	      C		  GND		pulse	0	  Vdd	0	   50p	 50p	2n	4n 
** signals S & R 
** for hold an setup uncomment this
*Vs	      S		  GND		pulse	0	  Vdd	5n   50p  50p	2ns	
** for rise and fall uncomment this
*Vs	      S		  GND		pulse	0	  Vdd	40n  50p  50p	20n	

*Vr	      R		  GND		pulse	0	  Vdd	10n  50p  50p	20n 100n 	

** uncomment for Glitch
Vs	      S		  GND		pulse	0	  Vdd	4.5n	50p	50p	500p	
Vr		    R		  GND		pulse	0	  Vdd	1n	50p	50p	2n	

**** Subkits ****

**** Static CMOS Inverter with 5f capacitor in output ****
.SUBCKT Inverter IN VDD GND OUT
M1   OUT	IN	 GND	GND	 nmos	l='Lmin'	w='Wn'
M2	 OUT	IN	 VDD	VDD	 pmos	l='Lmin'	w='Wp'	
C1	 OUT	GND  5f
.ENDS Inverter

**** Static CMOS NAND with 10f capacitor in output ****
.SUBCKT NAND A B VDD GND OUT
M3	OUT 	 A	   VDD	 VDD	  pmos	  l='Lmin'	w='Wp'	
M4	OUT 	 B	   VDD	 VDD	  pmos	  l='Lmin'	w='Wp'
M5	OUT 	 A	   C     C	    nmos	  l='Lmin'	w='Wn'
M6	C      B	   GND	 GND	  nmos	  l='Lmin'	w='Wn'
C2	OUT 	 GND	 10f
.ENDS NAND

**** Static CMOS NOR with 10f capacitor in output ****
.SUBCKT NOR A B VDD GND OUT
M7	    C	    A	  VDD	     VDD  	  pmos	l='Lmin'	w='Wp'	
M8    	OUT	  B 	C	       C	      pmos	l='Lmin'	w='Wp'
M9	    OUT	  A	  GND	     GND	    nmos	l='Lmin'	w='Wn'
M10	    OUT	  B 	GND	     GND	    nmos	l='Lmin'	w='Wn'
C3	    OUT	  GND	10f
.ENDS NOR

**** Circuit ****
** And
X1	   S	  C	    Vs	    GND	  m1	    NAND
X2	   m1	  Vs	  GND	    n1	          Inverter
** And
X3	   R	  C	    Vs	    GND	  m2	    NAND
X4	   m2	  Vs    GND	    n2	          Inverter
** Nors
X5	   n1	  Q	    Vs	    GND	  QBar	  NOR
X6	   n2	  QBar	Vs	    GND	  Q	      NOR

**** Analysis ****
** for hold and setup uncomment this & for glitch
.tran 	1p 10ns
** for rise and fall uncomment this
*.tran 	1p 200ns

**** Measurements ****

.end