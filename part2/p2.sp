* Ehsan Jahangirzadeh 810194554
* CA4 clocked DFF Transmition gates

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=45n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1V
+beta=2
+Wp='2*Lmin*beta'
+Wn='2*Lmin'
.temp   25

**** Source Voltage ****

VSupply		Vs		GND		DC		Vdd

Vc		    C		    GND		pulse		0		  Vdd		2n		  50p	50p	  2n	  4n

** for parameters uncomment
*Vd		    D		    GND		pulse		0		  Vdd		1.5n		50p	50p	  1n    2.5n
** for 1-1
*Vd		    D		    GND		pulse		0			vdd	  5.5n		50p	50p	  1n
** for 0-0
Vd		    D		    GND		pulse		0			vdd	  7.5n		50p	50p	  2n

**** Subkits ****

**** Static CMOS Inverter with 5f capacitor in output ****
.SUBCKT Inverter IN VDD GND OUT
M1   OUT	IN	 GND	GND	 nmos	l='Lmin'	w='Wn'
M2	 OUT	IN	 VDD	VDD	 pmos	l='Lmin'	w='Wp'	
C1	 OUT	GND  5f
.ENDS Inverter

**** Transmition Gate with 5f capacitor in output ****
.SUBCKT TRANSGATE IN CLK CLKB VDD GND OUT
M3	OUT	CLK	    IN	VDD 	  pmos	l='Lmin'	w='Wp'	
M4	OUT	CLKB	  IN	GND	    nmos	l='Lmin'	w='Wn'
C2	OUT	GND	    5f
.ENDS TRANSGATE

**** Circuit ****
X1	Cbar8	      Vs	  GND	    Cbar	            Inverter
** for more ~clk skew uncomment lines below
X6	C	          Vs	  GND	    Cbar1	            Inverter
X7	Cbar1	      Vs	  GND	    Cbar2	            Inverter
X8	Cbar2	      Vs	  GND	    Cbar3	            Inverter
X9	Cbar3       Vs	  GND	    Cbar4	            Inverter
X10	Cbar4       Vs	  GND	    Cbar5	            Inverter
X11	Cbar5       Vs	  GND	    Cbar6	            Inverter
X12	Cbar6       Vs	  GND	    Cbar7	            Inverter
X13	Cbar7       Vs	  GND	    Cbar8	            Inverter

** left
X2	D	      C	    Cbar	  Vs	  GND	  m1        TRANSGATE
X3	m1	    Vs	  GND	    out1	                Inverter

** right
X4	out1	  Cbar	C	      Vs	  GND	  m2	      TRANSGATE
X5	m2	    Vs	  GND     Q	                    Inverter

**** Analysis ****
.tran 	1p 15n

**** Measurements ****

.end