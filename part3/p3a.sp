* Ehsan Jahangirzadeh 810194554
* CA4 clocked DFF Transmition gates

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=45n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1V
+beta=2
+Wp='4*Lmin*beta'
+Wn='2*Lmin'
.temp   25

**** Source Voltage ****

VSupply		    Vs  GND		DC		Vdd
Vc		        C		GND		pulse	0	  Vdd	   0	  50p  	  50p	  2n	4n
Vin		        D		GND		pulse	0	  Vdd	   3n	  50p	    50p	  2n	

**** Subkits ****

**** Static CMOS Inverter with 5f capacitor in output ****
.SUBCKT Inverter IN VDD GND OUT
M1   OUT	IN	 GND	GND	 nmos	l='Lmin'	w='Wn'
M2	 OUT	IN	 VDD	VDD	 pmos	l='Lmin'	w='Wp'	
C1	 OUT	GND  5f
.ENDS Inverter

**** Static CMOS NAND with 5f capacitor in output ****
.SUBCKT NAND A B VDD GND OUT
M3	OUT 	 A	   VDD	 VDD	  pmos	  l='Lmin'	w='Wp'	
M4	OUT 	 B	   VDD	 VDD	  pmos	  l='Lmin'	w='Wp'
M5	OUT 	 A	   C     C	    nmos	  l='Lmin'	w='Wn'
M6	C      B	   GND	 GND	  nmos	  l='Lmin'	w='Wn'
C2	OUT 	 GND	 5f
.ENDS NAND

**** Static CMOS NAND3 with 5f capacitor in output ****
.SUBCKT NAND3 A B C VDD GND OUT
M7	OUT	  A	  D1	    D1	  nmos	l='Lmin'	w='Wn'
M8	D1	  B	  D2	    D2	  nmos	l='Lmin'	w='Wn'
M9	D2	  C	  GND     GND	  nmos	l='Lmin'	w='Wn'
M10	OUT	  A	  VDD	    VDD   pmos	l='Lmin'	w='Wp'
M11	OUT	  C	  VDD   	VDD   pmos	l='Lmin'	w='Wp'		
M12	OUT	  B	  VDD	    VDD   pmos	l='Lmin'	w='Wp'
C3	OUT	  GND	5f
.ENDS NAND3

**** Circuit ****
X1	m3	 D	    Vs	 GND	  m4  	        NAND
X2	m2	 Qbar	  Vs	 GND	  Q	            NAND
X3	m4	 m2   	Vs	 GND	  m1  	        NAND
X4	m2	 C	    m4	 Vs	    GND	    m3  	NAND3
X5	m1	 C	    Vs	 GND	  m2	          NAND
X6	m3	 Q	    Vs	 GND	  Qbar	        NAND

**** Analysis ****
.tran 	1p 15n

**** Measurements ****

.end