* Ehsan Jahangirzadeh 810194554
* CA4 clocked DFF Transmition gates

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=45n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1V
+beta=2
+Wp='4*Lmin*beta'
+Wn='2*Lmin'
.temp   25

**** Source Voltage ****

VSupply		Vs		gnd		DC		  Vdd
*Vc		    C		  GND		pulse	  0	  Vdd	  2n 50p  50p	  2n	  4n
** for more max frequency test uncomment 
Vc		    C		  gnd		pulse	  0	  Vdd	  0	 50p  50p	  0.25n	0.5n

Vin		    D		  gnd		pulse	  0	  Vdd	  0	 50p	50p 	1n	  2n
Vb		    BIN		gnd		DC	    0

**** Subkits ****

**** Static CMOS Inverter with 10f capacitor in output ****
.SUBCKT Inverter IN VDD GND OUT
M1   OUT	IN	 GND	GND	 nmos	l='Lmin'	w='Wn'
M2	 OUT	IN	 VDD	VDD	 pmos	l='Lmin'	w='Wp'	
C1	 OUT	GND  10f
.ENDS Inverter

**** Static CMOS NAND with 5f capacitor in output ****
.SUBCKT NAND A B VDD GND OUT
M3	OUT 	 A	   VDD	 VDD	  pmos	  l='Lmin'	w='Wp'	
M4	OUT 	 B	   VDD	 VDD	  pmos	  l='Lmin'	w='Wp'
M5	OUT 	 A	   C     C	    nmos	  l='Lmin'	w='Wn'
M6	C      B	   GND	 GND	  nmos	  l='Lmin'	w='Wn'
C2	OUT 	 GND	 5f
.ENDS NAND

**** Static CMOS NAND3 with 5f capacitor in output ****
.SUBCKT NAND3 A B C VDD GND OUT
M7	OUT	  A	  D1	    D1	  nmos	l='Lmin'	w='Wn'
M8	D1	  B	  D2	    D2	  nmos	l='Lmin'	w='Wn'
M9	D2	  C	  GND     GND	  nmos	l='Lmin'	w='Wn'
M10	OUT	  A	  VDD	    VDD   pmos	l='Lmin'	w='Wp'
M11	OUT	  C	  VDD   	VDD   pmos	l='Lmin'	w='Wp'		
M12	OUT	  B	  VDD	    VDD   pmos	l='Lmin'	w='Wp'
C3	OUT	  GND	5f
.ENDS NAND3

**** Static CMOS Combinational circuit ****
.SUBCKT COMB A Abar B Bbar VDD GND OUT
M13	OUT	  Abar	k	  k	  nmos	l='Lmin'	w='Wn'	
M14	k	    Bbar	GND	GND	nmos	l='Lmin'	w='Wn'	
M15	OUT	  A	    ka	ka	nmos	l='Lmin'	w='Wn'	
M16	ka	  B	    GND	GND	nmos	l='Lmin'	w='Wn'	
M17	kb	  A	    VDD	VDD	pmos	l='Lmin'	w='Wp'	
M18	OUT	  Bbar	kb	kb	pmos	l='Lmin'	w='Wp'	
M19	kb	  B	    VDD	VDD	pmos	l='Lmin'	w='Wp'	
M20	OUT	  Abar	kb	kb	pmos	l='Lmin'	w='Wp'	
C5	OUT	  GND	  10f
.ENDS COMB

**** Circuit ****
** input seq circuit
X1	m3	  D	      Vs	 GND	  m4  	              NAND
X2	m2	  Qbar1	  Vs	 GND	  Q1	                NAND
X3	m4	  m2   	  Vs	 GND	  m1  	              NAND
X4	m2	  C	      m4	 Vs	    GND	    m3  	      NAND3
X5	m1	  C	      Vs	 GND	  m2	                NAND
X6	m3	  Q1	    Vs	 GND	  Qbar1	              NAND
** output seq circuit
X7	m7	  D2	    Vs	 GND	  m8  	              NAND
X8	m6	  Qbar2	  Vs	 GND	  Q2	                NAND
X9	m8	  m6   	  Vs	 GND	  m5  	              NAND
X10	m6	  C	      m8	 Vs	    GND	    m7  	      NAND3
X11	m5	  C	      Vs	 GND	  m6	                NAND
X12	m7	  Q2	    Vs	 GND	  Qbar2	              NAND
** inputs complement
X13	BIN	  Vs	    GND	 bnot	                      Inverter
X14	Q1	  Vs	    GND	 anot	                      Inverter
** combinational circuit
X19 Q1    anot    BIN  bnot   Vs      GND   Out1  COMB  

** output buffer
X15	OUT1	Vs	    GND	 l1	                        Inverter
X16	l1	  Vs	    GND	 l2                         Inverter
X17	l2	  Vs	    GND	 l3	                        Inverter
X18	l3	  Vs	    GND	 D2	                        Inverter

**** Analysis ****
.tran 	1p 15n

**** Measurements ****

.end